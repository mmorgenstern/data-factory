/**
 * @fileoverview gRPC-Web generated client stub for 
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = require('./currency_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.CurrencyClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.CurrencyPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.RateRequest,
 *   !proto.RateResponse>}
 */
const methodDescriptor_Currency_GetRate = new grpc.web.MethodDescriptor(
  '/Currency/GetRate',
  grpc.web.MethodType.UNARY,
  proto.RateRequest,
  proto.RateResponse,
  /**
   * @param {!proto.RateRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.RateResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.RateRequest,
 *   !proto.RateResponse>}
 */
const methodInfo_Currency_GetRate = new grpc.web.AbstractClientBase.MethodInfo(
  proto.RateResponse,
  /**
   * @param {!proto.RateRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.RateResponse.deserializeBinary
);


/**
 * @param {!proto.RateRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.RateResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.RateResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.CurrencyClient.prototype.getRate =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/Currency/GetRate',
      request,
      metadata || {},
      methodDescriptor_Currency_GetRate,
      callback);
};


/**
 * @param {!proto.RateRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.RateResponse>}
 *     Promise that resolves to the response
 */
proto.CurrencyPromiseClient.prototype.getRate =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/Currency/GetRate',
      request,
      metadata || {},
      methodDescriptor_Currency_GetRate);
};


module.exports = proto;

