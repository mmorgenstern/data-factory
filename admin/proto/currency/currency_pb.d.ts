import * as jspb from 'google-protobuf'



export class RateRequest extends jspb.Message {
  getBase(): string;
  setBase(value: string): RateRequest;

  getDestination(): string;
  setDestination(value: string): RateRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RateRequest): RateRequest.AsObject;
  static serializeBinaryToWriter(message: RateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RateRequest;
  static deserializeBinaryFromReader(message: RateRequest, reader: jspb.BinaryReader): RateRequest;
}

export namespace RateRequest {
  export type AsObject = {
    base: string,
    destination: string,
  }
}

export class RateResponse extends jspb.Message {
  getRate(): number;
  setRate(value: number): RateResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RateResponse.AsObject;
  static toObject(includeInstance: boolean, msg: RateResponse): RateResponse.AsObject;
  static serializeBinaryToWriter(message: RateResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RateResponse;
  static deserializeBinaryFromReader(message: RateResponse, reader: jspb.BinaryReader): RateResponse;
}

export namespace RateResponse {
  export type AsObject = {
    rate: number,
  }
}

