import * as grpcWeb from 'grpc-web';

import * as currency_currency_pb from '../currency/currency_pb';


export class CurrencyClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getRate(
    request: currency_currency_pb.RateRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.Error,
               response: currency_currency_pb.RateResponse) => void
  ): grpcWeb.ClientReadableStream<currency_currency_pb.RateResponse>;

}

export class CurrencyPromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  getRate(
    request: currency_currency_pb.RateRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<currency_currency_pb.RateResponse>;

}

