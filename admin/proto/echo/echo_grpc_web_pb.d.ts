import * as grpcWeb from 'grpc-web';

import * as echo_echo_pb from '../echo/echo_pb';


export class EchoServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  echo(
    request: echo_echo_pb.EchoRequest,
    metadata: grpcWeb.Metadata | undefined,
    callback: (err: grpcWeb.Error,
               response: echo_echo_pb.EchoResponse) => void
  ): grpcWeb.ClientReadableStream<echo_echo_pb.EchoResponse>;

}

export class EchoServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  echo(
    request: echo_echo_pb.EchoRequest,
    metadata?: grpcWeb.Metadata
  ): Promise<echo_echo_pb.EchoResponse>;

}

