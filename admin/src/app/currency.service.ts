import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';
import {RateRequest} from '../../proto/currency/currency_pb';
import {CurrencyPromiseClient} from '../../proto/currency/currency_grpc_web_pb';
import {BehaviorSubject} from 'rxjs';

@Injectable({providedIn: 'root'})
export class CurrencyService {
  private client = new CurrencyPromiseClient(environment.serviceHost);

  rate$ = new BehaviorSubject<number>(1.0);

  rate(base: string, destination: string) {
    const req = new RateRequest();
    req.setBase(base);
    req.setDestination(destination);
    this.client.getRate(req).then(res => {
      this.rate$.next(res.getRate());
    }).catch(err => {
      console.error(err);
      this.rate$.next(1.0);
    });
  }
}
