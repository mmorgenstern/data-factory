import {Injectable} from '@angular/core';
import {EchoServiceClient} from '../../proto/echo/echo_grpc_web_pb';
import {environment} from '../environments/environment';
import {BehaviorSubject} from 'rxjs';
import {EchoRequest} from '../../proto/echo/echo_pb';

@Injectable({providedIn: 'root'})
export class EchoService {
  private client = new EchoServiceClient(environment.serviceHost);

  message$ = new BehaviorSubject<string>('');

  echo(message: string) {
    const req = new EchoRequest();
    req.setMessage(message);
    this.client.echo(req, undefined, (err, res) => {
      if (err) {
        console.error(err);
        this.message$.next('');
      } else {
        console.info('data => ', res.getMessage());
        this.message$.next(res.getMessage());
      }
    });
  }
}
