import {AfterContentInit, Component} from '@angular/core';
import {CurrencyService} from "./currency.service";
import {EchoService} from "./echo.service";

@Component({
  selector: 'app-root',
  template: `
    <h1>Rate: {{currencyService.rate$ | async}}</h1>
    <div>
      <input #message type="text" />
      <button (click)="sendMessage(message.value)">Send</button>
    </div>
    <p>{{this.echoService.message$ | async}}</p>
  `,
})
export class AppComponent implements AfterContentInit {
  constructor(public currencyService: CurrencyService,
              public echoService: EchoService) {
  }

  ngAfterContentInit() {
    this.currencyService.rate('GPB', 'USD');
  }

  sendMessage(value: string) {
    this.echoService.echo(value);
  }
}
