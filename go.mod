module gitlab.com/mmorgenstern/data-factory

go 1.16

require (
	github.com/fatih/color v1.12.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/hashicorp/go-hclog v0.16.2 // indirect
	github.com/mattn/go-isatty v0.0.13 // indirect
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
	golang.org/x/sys v0.0.0-20210806184541-e5e7981a1069 // indirect
	google.golang.org/genproto v0.0.0-20210805201207-89edb61ffb67 // indirect
	google.golang.org/grpc v1.39.1 // indirect
)
