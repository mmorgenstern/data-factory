package main

import (
	"github.com/hashicorp/go-hclog"
	"gitlab.com/mmorgenstern/data-factory/pkg"
	"gitlab.com/mmorgenstern/data-factory/protos/currency"
	"gitlab.com/mmorgenstern/data-factory/protos/echo"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"net"
	"os"
)

func main() {
	log := hclog.Default()
	gs := grpc.NewServer()
	currency.RegisterCurrencyServer(gs, pkg.NewCurrency(log))
	echo.RegisterEchoServiceServer(gs, pkg.NewEcho(log))

	reflection.Register(gs)

	l, err := net.Listen("tcp", ":9092")
	if err != nil {
		log.Error("Unable to listen", "error", err)
		os.Exit(1)
	}
	err = gs.Serve(l)
	if err != nil {
		log.Error("Unable to start Server", "error", err)
	}
}
