.PHONY: protos dockerup

protos:
	protoc -I protos/ protos/currency/currency.proto --go_out=plugins=grpc:protos
	protoc -I protos/ protos/currency/currency.proto --js_out=import_style=commonjs:./admin/proto --grpc-web_out=import_style=commonjs+dts,mode=grpcwebtext:./admin/proto
	protoc -I protos/ protos/echo/echo.proto --go_out=plugins=grpc:protos
	protoc -I protos/ protos/echo/echo.proto --js_out=import_style=commonjs:./admin/proto --grpc-web_out=import_style=commonjs+dts,mode=grpcwebtext:./admin/proto
