package pkg

import (
	"context"
	"github.com/hashicorp/go-hclog"
	"gitlab.com/mmorgenstern/data-factory/protos/currency"
)

type Currency struct {
	log hclog.Logger
}

func NewCurrency(l hclog.Logger) *Currency {
	return &Currency{l}
}

func (c *Currency) GetRate(ctx context.Context, req *currency.RateRequest) (*currency.RateResponse, error) {
	c.log.Info("Handle GetRate", "base", req.GetBase(), "destination", req.GetDestination())
	return &currency.RateResponse{Rate: 0.5}, nil
}
