package pkg

import (
	"context"
	"github.com/hashicorp/go-hclog"
	"gitlab.com/mmorgenstern/data-factory/protos/echo"
)

type Echo struct {
	log hclog.Logger
}

func NewEcho(l hclog.Logger) *Echo {
	return &Echo{log: l}
}

func (e *Echo) Echo(ctx context.Context, req *echo.EchoRequest) (*echo.EchoResponse, error) {
	e.log.Info("Echo", "message", req.GetMessage())
	return &echo.EchoResponse{Message: req.GetMessage()}, nil
}
